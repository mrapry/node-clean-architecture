import Sequelize from 'sequelize'
import dbConfig from '../../../.env.json'

import CartModel from '../../../src/service/v1/cart/model/Cart'

let instance = null
export default class DBConnection {
    constructor() {
        if (!instance) {
            instance = this
        }
        let env = process.env.NODE_ENV || 'localhost'
        let config = dbConfig.db
        const sequelize = new Sequelize(config.database, config.username, config.password, config)
        var dburl =  config.dialect+'://'+config.username+'@'+config.host+'/'+config.database
        console.log('[DB] connected', dburl)

        // Model schema initialization goes here...
        this.sequelize = sequelize
        this.cart = CartModel.generate(this.sequelize)

        // Association schema goes here...

        return instance
    }
}
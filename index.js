import express from 'express'
import http from 'http'
import bodyParser from 'body-parser'
import logger from 'morgan'
import DBConnection from './config/db/postgres/postgres'
import MainHTTP from './MainHTTP'
import config from './.env.json'

const app = express()
app.use(bodyParser.json({limit: '10mb'}))
app.use(logger('dev'))
app.use('/', MainHTTP.router)

const db = new DBConnection()
const port = config.port
const server = http.createServer(app)
db.sequelize.sync().then(() => {
    server.listen(port, () => {
        console.log('[CLEAN-ARCH] service listening on port', port)
    })
})
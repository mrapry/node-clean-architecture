import Sequelize from 'sequelize'
import dbConfig from '../configs/db.config.json'

import CartModel from '../src'

let instance = null
export default class DBConnection {
    constructor() {
        if (!instance) {
            instance = this
        }
        let env = process.env.NODE_ENV || 'localhost'
        let config = dbConfig[env]
        const sequelize = new Sequelize(config.database, config.username, config.password, config)
        var dburl =  config.dialect+'://'+config.username+'@'+config.host+'/'+config.database
        console.log('[DB] connected', dburl)

        // Model schema initialization goes here...
        this.sequelize = sequelize
        this.event = Event.generate(this.sequelize)

        // Association schema goes here...

        return instance
    }
}
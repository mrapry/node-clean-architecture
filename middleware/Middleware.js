import jwt from 'jsonwebtoken'
import config from '../.env.json'
import ResponseMessage from '../helper/ResponseMessage'
import HTTPHelper from '../helper/HTTPHelper'

class Middleware {
    validateToken(byPass = false) {
        return (req, res, next) => {
            try {
                let token = req.headers['authorization'];
                if (token) {
                    token = token.replace('Bearer ','')
                }
                jwt.verify(token, config.secretKey, (err, decoded) => {
                    if (err) {
                        let response = ResponseMessage.doResponse(401, 'Unauthorized')
                        res.status(401).send(response)
                    } else {
                        if (byPass == true) {
                            if (decoded.isAdmin == true) {
                                req.token = decoded
                                return next()
                            } else {
                                let response = ResponseMessage.doResponse(401, 'Unauthorized')
                                res.status(401).send(response)
                            }
                        } else {
                            req.token = decoded
                            return next()
                        }
                    }
                })
            } catch(error) {
                let response = ResponseMessage.doResponse(401, 'Unauthorized')
                res.status(401).send(response)
            }
        }
    }
}
export default new Middleware()
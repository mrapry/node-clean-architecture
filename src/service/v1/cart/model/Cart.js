import Sequelize from 'sequelize'

class Cart {
    constructor(data) {
    }
    tableName() {
        return 'cart'
    }
    schema() {
        return {
            id: {
                primaryKey: true,
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4
            },
            title:{
                type: Sequelize.STRING,
                allowNull: false,
            },
            isActive:{
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        }
    }
    generate(instance) {
        console.log('Generate ' + this.tableName() + ' table...')
        this.instance = instance.define(this.tableName(), this.schema())
        return this.instance
    }

    get title() {
        return this._title
    }
    set title(title) {
        this._title = title
    }
    get isActive() {
        return this._isActive
    }
    set isActive(isActive) {
        this._isActive = isActive
    }
}
export default new Cart()

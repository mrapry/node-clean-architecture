import express from 'express'
import CartInterface from '../../../service/v1/cart/usecase/CartInterface'
import Middleware from '../../../../middleware/Middleware'

class CartController {
    get handler() {
        const router = express.Router({
            mergeParams: true
        })
        //router.get('/', Middleware.validateToken(), this.getAllCart())
        router.get('/', this.getAllCart())
        return router
    }

    getAllCart() {
        return CartInterface.getAllCart()
    }
}
export default new CartController
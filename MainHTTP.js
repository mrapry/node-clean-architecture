import express from 'express'
import CartController from './src/delivery/v1/Cart/CartController'

class MainHTTPRouter {
    get router() {
        const router = express.Router({
            mergeParams: true
        })
        router.use('/v1/cart', CartController.handler)
        return router
    }
}
export default new MainHTTPRouter()